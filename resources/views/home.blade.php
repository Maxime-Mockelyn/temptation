@extends("layouts.app")

@section("css")

@endsection

@section("content")
    <section id="home" class="home-slider">
        <div class="container">
            <div class="flexslider">
                <ul class="slides">
                    <li class="slide">
                        <img alt="" src="/assets/images/slide/banner.png" />
                    </li>
                </ul>
            </div>
        </div>
    </section>

    <section class="section-home-products">
        <div class="container">
            <div class="controls-holder nav-tabs">
                <ul class="inline">
                    <li class="active"><a data-toggle="tab" href="#hot-products">Nos Derniers Produits</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div id="hot-products" class="products-holder active tab-pane ">
                    <div class="row-fluid">
                        @foreach(\App\Repository\Product\ProductRepository::staticAll() as $product)
                        <div id="produit">
                            <div class="span3">
                                <div class="product-item">

                                    <a href="{{ route('product', [$product->category->id, $product->id]) }}">
                                        <img alt="" src="/assets/images/product/{{ $product->id }}.jpg" />
                                        <h1>{{ $product->name }}</h1>
                                    </a>
                                    <div class="tag-line">
                                        <span>{{ str_limit($product->recapitulatif, '50', '...') }}</span>
                                    </div>
                                    <div class="price">
                                        {{ formatCurrency($product->price) }}
                                    </div>
                                    <form id="addProduct" action="{{ route('Cart.addProduct') }}" method="POST">
                                        @csrf
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <button type="submit" id="btnAddProduct" class="cusmo-btn add-button" >Ajouter au panier</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>

                </div>
            </div>

        </div>
    </section>
@endsection

@section("scripts")

@endsection()