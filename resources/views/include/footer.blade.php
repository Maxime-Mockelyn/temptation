<!--<section class="section-footer">
    <div class="container">
        <div class="row-fluid">
            <div class="span3">
                <div class="footer-links-holder">
                    <h2>informations</h2>
                    <ul>
                        <li><a href="#" >our blog</a></li>
                        <li><a href="#" >about our shop</a></li>
                        <li><a href="#" >secure shopping</a></li>
                        <li><a href="#" >privacy policy</a></li>
                        <li><a href="#" >delivery informations</a></li>
                    </ul>
                </div>
            </div>
            <div class="span3">
                <div class="footer-links-holder">
                    <h2>customer care</h2>
                    <ul>
                        <li><a href="#" >contact us</a></li>
                        <li><a href="#" >site map</a></li>
                        <li><a href="#" >top sales & bestsellers</a></li>
                        <li><a href="#" >gift vouchers</a></li>
                        <li><a href="#" >best sellers</a></li>
                    </ul>
                </div>
            </div>
            <div class="span3">
                <div class="footer-links-holder">
                    <h2>your account</h2>
                    <ul>
                        <li><a href="#" >order status</a></li>
                        <li><a href="#" >my wishlist</a></li>
                        <li><a href="#" >delivery address</a></li>
                        <li><a href="#" >order history</a></li>
                        <li><a href="#" >newsletter</a></li>
                    </ul>
                </div>
            </div>
            <div class="span3">
                <div class="footer-links-holder">
                    <h2>get in touch</h2>
                    <p>
                        Cosmetico Shop<br>
                        Good Town 122, Beaty Centre<br>
                        (011) 212 222 22
                    </p>
                    <ul class="inline social-icons">
                        <li><a href="#" class="icon-facebook" ></a></li>
                        <li><a href="#" class="icon-google-plus" ></a></li>
                        <li><a href="#" class="icon-rss" ></a></li>

                        <li><a href="#" class="icon-linkedin" ></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>-->
<section class="section-copyright">
    <div class="container">
        <div class="copyright pull-left">
            <p>
                <strong>© OUIBIENETRE 2019</strong>. All rights reserved.<br>
            </p>
        </div>
        <div class="copyright-links pull-right">
            <ul class="inline">
                <li><a href="#">Politique de Confidentialité</a></li>
                <li><a href="#">Conditions générales de ventes</a></li>
            </ul>
        </div>
    </div>
</section>