@extends("layouts.app")

@section("css")
    <link href="/assets/css/chosen.css" rel="stylesheet">
@endsection

@section("content")
    <section class="section-shopping-cart">
        <div class="container">
            <div class="row-fluid">

                <div class="span12">
                    <div class="page-content shopping-cart-page ">

                        <table class="table " id="cart">
                            <thead>
                            <tr>
                                <th class="span2"></th>
                                <th class="span5">Produit</th>
                                <th class="span2 price-column">Prix</th>
                                <th class="span2">Quantité</th>
                                <th class="span1 price-column">total</th>
                                <th class="span2">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($cart->products as $product)
                                <tr class="productTable">
                                    <td>
                                        <div class="thumb">
                                            <img alt="" src="/assets/images/product/{{ $product->product->id }}.jpg" />
                                        </div>
                                    </td>
                                    <td>
                                        <div class="desc">
                                            <input type="hidden" id="productCart_id" name="productCart_id" value="{{ $product->id }}">
                                            <h3>{{ $product->product->name }}</h3>
                                            <div class="tag-line">
                                                {!! str_limit($product->product->description, 100) !!}
                                            </div>
                                            <div class="pid">Référence: {{ $product->product->reference }}</div>
                                        </div>

                                    </td>
                                    <td>

                                        <div class="price">
                                            {{ formatCurrency($product->product->price) }}
                                        </div>

                                    </td>
                                    <td>
                                        <div class="quantity">
                                            <select id="selectQuantity" class="chosen-select" data-product="{{ $product->id }}" data-cart="{{ $cart->id }}">
                                                <option value="{{ $product->qte }}">{{ $product->qte }}</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>

                                            </select>
                                        </div>
                                    </td>

                                    <td>

                                        <div class="price">
                                            {{ formatCurrency($product->total_price) }}
                                        </div>

                                    </td>

                                    <td>

                                        <div class="delete">
                                            <a class="close-btn" href="#"></a>
                                        </div>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="buttons-holder">
                            <button id="refresh" class="cusmo-btn gray narrow"><i class="icon icon-refresh"></i> Rafraichir la page</button>
                            <a class="cusmo-btn gray narrow" href="{{ redirect()->back() }}">Continuer mon shopping</a>
                            <a class="cusmo-btn narrow" href="{{ route("Checkout.index") }}">Commander</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--<section class="section-homepage-subscribe">
        <div class="container">
            <div class="big-circle">

                get the
                <div class="big"><span>$</span>10</div>
                cupon

            </div>
            <div class="offer-text">
                Sign in for our newsletter and recieve a ten dollars cupon
            </div>
            <div class="email-holder">

                <div class="email-field">

                    <form>
                        <input class=" required email" name="email" data-placeholder="Enter here your email address..." />
                        <button class="newsletter-submit-btn" type="submit" value=""><i class="icon-plus"></i></button>

                    </form>

                </div>
            </div>
        </div>
    </section>-->
@endsection

@section("scripts")
    <script type="text/javascript" src="/assets/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.raty.min.js"></script>
    <script type="text/javascript" src="/assets/js/chosen.jquery.min.js"></script>
    <script type="text/javascript">
        (function ($) {
            $("#cart").on("change", "#selectQuantity", function (e) {
                e.preventDefault()
                let table = $("#cart")
                let select = $(this);
                let product = select.attr('data-product')
                let cart = select.attr('data-cart')
                let price = $(".productTable").find('input[name="amount-price"]')

                $.ajax({
                    url: '/cart/updateProduct?product='+product+'&qte='+select.val()+'&cart='+cart,
                    method: "GET",
                    success: function (data) {
                        //console.log(data)
                        window.location.href='/cart'
                    }
                });

                console.log("table", table)
                console.log("select", select.val())
                console.log("select", product)
                console.log("price", price.val())
            })
            $("#refresh").on('click', function (e) {
                e.preventDefault()
                window.location.href='/cart'
            })
        })(jQuery)
    </script>
@endsection