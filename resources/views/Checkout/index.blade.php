@extends("layouts.app")

@section("css")
    <link href="/assets/css/chosen.css" rel="stylesheet">
@endsection

@section("content")
    <section class="section-checkout">
        <div class="container">
            <div class="row-fluid">
                <div class="form-holder">
                    <div class="span12">
                        <h1 class="title">Passage de commande</h1>
                        <form action="{{ route('Checkout.checkout') }}" method="POST">
                            <div class="control-group">
                                <div class="controls">
                                    <h3><u>Identification</u></h3>
                                    <br>
                                    <div class="form-label ">Nom & Prénom</div>
                                    <input type="text" id="name"   name="name"  class="required span12 cusmo-input"  />
                                    <div class="form-label ">Adresse Mail</div>
                                    <input type="text" id="email"   name="email"  class="required span12 cusmo-input"  />
                                    <div class="form-label ">Numéro de Téléphone</div>
                                    <input type="text" id="telephone"   name="telephone"  class="required span12 cusmo-input"  />
                                    <hr />
                                    <h3><u>Livraison & Facturation</u></h3>
                                    <br>
                                    <div class="form-label ">Adresse Postal</div>
                                    <input type="text" id="adresse"   name="adresse"  class="required span12 cusmo-input"  />
                                </div>
                                <div class="controls">
                                    <div class="form-label ">Code Postal</div>
                                    <input type="text" id="code_postal"   name="code_postal"  class="required span4 cusmo-input"  />
                                    <div class="form-label ">Ville</div>
                                    <input type="text" id="ville"   name="ville"  class="required span12 cusmo-input"  />
                                </div>
                                <hr />
                                <div class="controls">
                                    <h3><u>Paiement de la commande</u></h3>
                                    <br>
                                    <table class="table">
                                        <tr>
                                            <td>Panier N°</td>
                                            <td>{{ $cart->token }}</td>
                                        </tr>
                                        <tr>
                                            <td>Total de la commande</td>
                                            <td>{{ formatCurrency($cart->total_cart) }}</td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section("scripts")
    <script type="text/javascript" src="/assets/js/bootstrap-slider.js"></script>
    <script type="text/javascript" src="/assets/js/jquery.raty.min.js"></script>
    <script src="/assets/js/jquery.icheck.min.js"></script>
    <script type="text/javascript" src="/assets/js/chosen.jquery.min.js"></script>
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection