(function ($) {
    $("#produit").on('click', "#btnAddProduct",function (e) {
        e.preventDefault()
        let form = $("#addProduct")
        let url = form.attr("action")
        let btn = $("#btnAddProduct")
        let data = form.serializeArray()



        $.ajax({
            url: url,
            method: "POST",
            data: data,
            success: function (data) {
                swal.fire({
                    title: "Ok !",
                    text: "Le Produit <strong>"+ data.product.name +"</strong> à été ajouté au panier",
                    type: "success",
                    confirmButtonText: 'Merci'
                })
            }
        })
    })
    $(".basket").on('click', '#deleteProductCart', function (e) {
        e.preventDefault()
        let btn = $(this)
        let url = btn.attr('href')

        $.ajax({
            url: url,
            success: function (data) {

            }
        })
    })
})(jQuery);