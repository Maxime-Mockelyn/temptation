<?php

namespace App\Http\Controllers\Cart;

use App\Repository\Cart\CartProductRepository;
use App\Repository\Cart\CartRepository;
use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var CartProductRepository
     */
    private $cartProductRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CartController constructor.
     * @param CartRepository $cartRepository
     * @param CartProductRepository $cartProductRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(CartRepository $cartRepository, CartProductRepository $cartProductRepository, ProductRepository $productRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {
        return view("cart", [
            "cart"  => $this->cartRepository->get(session()->get('_token'))
        ]);
    }

    public function createPanier()
    {
        $_token = session()->get('_token');
        if ($this->cartRepository->cartExist($_token) == false)
        {
            $cart = $this->cartRepository->create($_token);
            return $cart;
        }else{
            return $this->cartRepository->get($_token);
        }
    }

    public function addProduct(Request $request)
    {
        $cart = $this->cartRepository->get(session()->get('_token'));
        $product = $this->productRepository->get($request->product_id);
        if($this->cartProductRepository->product_exist($cart->id,$request->product_id) == true){
            $cartPro = $this->cartProductRepository->get($cart->id, $request->product_id);
            $qte = $cartPro->qte +1;
            $total_price = $product->price*$qte;
            $this->cartProductRepository->updateProduct(
                $cart->id,
                $request->product_id,
                $qte,
                $total_price
            );
        }else{
            $qte = 1;
            $total_price = $product->price*$qte;
            $this->cartProductRepository->createProduct(
                $cart->id,
                $request->product_id,
                $qte,
                $total_price
            );
        }

        $totalCart = $cart->total_cart+$total_price;
        $this->cartRepository->updateTotal(
            $cart->id,
            $totalCart
        );

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return response()->json([
                "product"   => [
                    "name"  => $product->name
                ]
            ]);
        }else{
            return redirect()->back();
        }

    }

    public function updateProduct(Request $request)
    {
        //dd($request->all());

        $cart = $this->cartRepository->get(session()->get('_token'));
        $product = $this->cartProductRepository->getById($request->product);

        $total_price = $product->product->price*$request->qte;
        $total_cart = $cart->total_cart-$product->total_price+$total_price;

        //dd($total_cart);

        $this->cartProductRepository->updateProduct(
            $request->cart,
            $product->product_id,
            $request->qte,
            $total_price
        );

        $this->cartRepository->updateTotal($request->cart, $total_cart);

        return response()->json();
    }

    public function deleteProduct($product_id)
    {
        $cart = $this->cartRepository->get(session()->get('_token'));
        $product = $this->cartProductRepository->getById($product_id);

        $total_cart = $cart->total_cart-$product->total_price;

        $this->cartRepository->updateTotal($cart->id, $total_cart);

        $this->cartProductRepository->delete($product_id);
    }
}
