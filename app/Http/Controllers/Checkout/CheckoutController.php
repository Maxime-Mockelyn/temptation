<?php

namespace App\Http\Controllers\Checkout;

use App\Repository\Cart\CartProductRepository;
use App\Repository\Cart\CartRepository;
use App\Repository\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Stripe\Checkout\Session;
use Stripe\Stripe;

class CheckoutController extends Controller
{
    /**
     * @var CartRepository
     */
    private $cartRepository;
    /**
     * @var CartProductRepository
     */
    private $cartProductRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CheckoutController constructor.
     * @param CartRepository $cartRepository
     * @param CartProductRepository $cartProductRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(CartRepository $cartRepository, CartProductRepository $cartProductRepository, ProductRepository $productRepository)
    {
        $this->cartRepository = $cartRepository;
        $this->cartProductRepository = $cartProductRepository;
        $this->productRepository = $productRepository;
    }

    public function index()
    {


        try {
            Stripe::setApiKey(env("STRIPE_SECRET"));



            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => [[
                    'name' => 'T-shirt',
                    'description' => 'Comfortable cotton t-shirt',
                    'images' => ['https://example.com/t-shirt.png'],
                    'amount' => 500,
                    'currency' => 'eur',
                    'quantity' => 1,
                ]],
                'success_url' => 'https://example.com/success',
                'cancel_url' => 'https://example.com/cancel',
            ]);

        }catch (\Exception $exception){

        }

        return view("Checkout.index", [
            "cart"  => $this->cartRepository->get(session()->get('_token'))
        ]);
    }
}
