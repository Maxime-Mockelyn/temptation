<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Cart\CartController;
use App\Repository\Product\ProductCategoryRepository;
use App\Repository\Product\ProductRepository;
use App\Repository\Product\ProductSubCategoryRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;
    /**
     * @var CartController
     */
    private $cartController;
    /**
     * @var ProductSubCategoryRepository
     */
    private $productSubCategoryRepository;

    /**
     * Create a new controller instance.
     *
     * @param ProductRepository $productRepository
     * @param ProductCategoryRepository $productCategoryRepository
     * @param CartController $cartController
     * @param ProductSubCategoryRepository $productSubCategoryRepository
     */
    public function __construct(ProductRepository $productRepository, ProductCategoryRepository $productCategoryRepository, CartController $cartController, ProductSubCategoryRepository $productSubCategoryRepository)
    {
        $this->productRepository = $productRepository;
        $this->productCategoryRepository = $productCategoryRepository;
        $this->cartController = $cartController;
        $this->productSubCategoryRepository = $productSubCategoryRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $this->cartController->createPanier();
        return view('home');
    }

    public function categorie($category_id)
    {

        //dd($this->productRepository->getForCategory($category_id));

        return view("categorie",[
            "category" => $this->productCategoryRepository->get($category_id),
            "products"  => $this->productRepository->getForCategory($category_id)
        ]);
    }

    public function subcategorie($categorie_id, $sub_id)
    {
        return view('subcategorie', [
            "category"  => $this->productCategoryRepository->get($categorie_id),
            "sub"       => $this->productSubCategoryRepository->get($sub_id),
            "products"  => $this->productRepository->getForSubcategorie($sub_id)
        ]);
    }

    public function product($category_id, $product_id)
    {
        return view("product", [
            "product"   => $this->productRepository->get($product_id)
        ]);
    }
}
