<?php

namespace App\Model\Product;

use App\Model\Cart\Cart;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(ProductCategory::class, 'category_id');
    }

    public function subcategory()
    {
        return $this->belongsTo(ProductSubCategory::class, 'subcategory_id');
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }
}
