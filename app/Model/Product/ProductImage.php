<?php

namespace App\Model\Product;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
