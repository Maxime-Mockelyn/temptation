<?php

namespace App\Model\Checkout;

use Illuminate\Database\Eloquent\Model;

class CheckoutPaiement extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function getDates()
    {
        return ["datePaiement"];
    }
}
