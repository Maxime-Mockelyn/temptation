<?php

namespace App\Model\Checkout;

use App\Model\Cart\Cart;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    protected $guarded = [];

    public function products(){return $this->hasMany(CheckoutProduct::class);}
    public function paiements(){return $this->hasMany(CheckoutPaiement::class);}
    public function shippings(){return $this->hasMany(CheckoutShipping::class);}

    public function cart(){return $this->belongsTo(Cart::class, 'cart_id');}
    public function user(){return $this->belongsTo(User::class, 'user_id');}
    public function status(){return $this->belongsTo(CheckoutStatus::class, 'status_id');}

    public function getDates()
    {
        return ["created_at", "updated_at", "dateCheckout"];
    }
}
