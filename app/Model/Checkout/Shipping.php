<?php

namespace App\Model\Checkout;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
