<?php
namespace App\Repository\Product;

use App\Model\Product\ProductCategory;

class ProductCategoryRepository
{
    /**
     * @var ProductCategory
     */
    private $productCategory;

    /**
     * ProductCategoryRepository constructor.
     * @param ProductCategory $productCategory
     */

    public function __construct(ProductCategory $productCategory)
    {
        $this->productCategory = $productCategory;
    }

    public function all()
    {
        return $this->productCategory->newQuery()
            ->get();
    }

    public static function staticAll()
    {
        $category = new ProductCategory();

        return $category->newQuery()->get();
    }

    public function get($category_id)
    {
        return $this->productCategory->newQuery()->find($category_id);
    }


}

        