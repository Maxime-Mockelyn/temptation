<?php
namespace App\Repository\Cart;

use App\Model\Cart\Cart;

class CartRepository
{
    /**
     * @var Cart
     */
    private $cart;

    /**
     * CartRepository constructor.
     * @param Cart $cart
     */

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    public function cartExist($_token)
    {
        $count = $this->cart->newQuery()->where('token', $_token)->get()->count();

        if($count == 0){
            return false;
        }else{
            return true;
        }
    }

    public function get($_token)
    {
        return $this->cart->newQuery()->where('token', $_token)->first()->load('products');
    }

    public static function getStaticCart()
    {
        $token = session()->get('_token');
        $cart = new Cart();
        return $cart->newQuery()->where('token', $token)->first()->load('products');
    }


    public function create($_token)
    {
        return $this->cart->newQuery()->create([
            "token" => $_token
        ]);
    }

    public function updateTotal($id, float $totalCart)
    {
        return $this->cart->newQuery()->find($id)
            ->update([
                "total_cart"    => $totalCart
            ]);
    }

}

        