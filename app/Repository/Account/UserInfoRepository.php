<?php
namespace App\Repository\Account;

use App\Model\Account\UserInfo;

class UserInfoRepository
{
    /**
     * @var UserInfo
     */
    private $userInfo;

    /**
     * UserInfoRepository constructor.
     * @param UserInfo $userInfo
     */

    public function __construct(UserInfo $userInfo)
    {
        $this->userInfo = $userInfo;
    }

}

