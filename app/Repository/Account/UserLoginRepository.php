<?php
namespace App\Repository\Account;

use App\Model\Account\UserLogin;

class UserLoginRepository
{
    /**
     * @var UserLogin
     */
    private $userLogin;

    /**
     * UserLoginRepository constructor.
     * @param UserLogin $userLogin
     */

    public function __construct(UserLogin $userLogin)
    {
        $this->userLogin = $userLogin;
    }

}

