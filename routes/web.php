<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);


Route::group(["prefix" => "category"], function (){
    Route::get('/{categorie_id}', ["as" => "categorie", "uses" => "HomeController@categorie"]);
    Route::get('/{categorie_id}/subs/{subs_id}', ["as" => "subcategorie", "uses" => "HomeController@subcategorie"]);
    Route::get('/{categorie_id}/{product_id}', ["as" => "product", "uses" => "HomeController@product"]);
});

Route::group(["prefix" => "cart", "namespace" => "Cart"], function (){
    Route::get('/', ["as" => "Cart.index", "uses" => "CartController@index"]);
    Route::post('createCart', ["as" => "Cart.createCart", "uses" => "CartController@createCart"]);
    Route::post('addProduct', ["as" => "Cart.addProduct", "uses" => "CartController@addProduct"]);
    Route::post('addWhitelist', ["as" => "Cart.addWhitelist", "uses" => "CartController@addWhitelist"]);
    Route::get('/deleteProduct/{product_id}', ["as" => "Cart.deleteProduct", "uses" => "CartController@deleteProduct"]);

    Route::get('/updateProduct', "CartController@updateProduct");
});

Route::group(["prefix" => "checkout", "namespace" => "Checkout"], function (){
    Route::get('/', ["as" => "Checkout.index", "uses" => "CheckoutController@index"]);
    Route::post('/', ["as" => "Checkout.checkout", "uses" => "CheckoutController@checkout"]);
});

Route::get('/code', "TestController@code");

Auth::routes();

