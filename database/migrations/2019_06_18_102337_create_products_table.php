<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id');
            $table->string('name');
            $table->integer('type')->default(0)->comment("0: Produit Standard |1: Pack de produit |2: Dématérialiser");
            $table->string('reference');
            $table->integer('quantity')->default(0);
            $table->string('price')->default(0);
            $table->string('recapitulatif')->nullable();
            $table->longText('description')->nullable();
            $table->longText('howUse')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
