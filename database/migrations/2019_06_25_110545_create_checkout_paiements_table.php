<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutPaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_paiements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('checkout_id');
            $table->integer('mode_id');
            $table->date('datePaiement');
            $table->string('reference');
            $table->string('total_paiement');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_paiements');
    }
}
